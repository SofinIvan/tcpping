package com.infobip.tcp.ping;

import com.infobip.tcp.ping.util.CommandLineParameters;
import com.infobip.tcp.ping.util.CommandLineReader;
import com.infobip.tcp.ping.execution.catcher.CatcherThread;
import com.infobip.tcp.ping.execution.pitcher.PitcherThread;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.ParseException;

@Slf4j
public class TCPPingStarter {

    public static void main(String[] args) throws ParseException {
        CommandLineParameters parameters = CommandLineReader.readParametersFromCommandLine(args);
        log.info(parameters.toString());
        if (parameters.isPitcher()) {
            new PitcherThread(parameters.getPort(),
                    parameters.getSize(),
                    parameters.getMps(),
                    parameters.getHostname())
                    .start();
        }
        if (parameters.isCatcher()) {
            new CatcherThread(parameters.getPort(),
                    parameters.getBind())
                    .start();
        }
    }

}
