package com.infobip.tcp.ping.exceptions;

public class ReceivingException extends RuntimeException {

    public ReceivingException() {
    }

    public ReceivingException(String message) {
        super(message);
    }

    public ReceivingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReceivingException(Throwable cause) {
        super(cause);
    }

    public ReceivingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
