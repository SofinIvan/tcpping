package com.infobip.tcp.ping.exceptions;

public class InvalidCommandLineArgumentsException extends RuntimeException {

    public InvalidCommandLineArgumentsException() {
    }

    public InvalidCommandLineArgumentsException(String message) {
        super(message);
    }

    public InvalidCommandLineArgumentsException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCommandLineArgumentsException(Throwable cause) {
        super(cause);
    }

    public InvalidCommandLineArgumentsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
