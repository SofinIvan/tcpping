package com.infobip.tcp.ping.execution.catcher;

import com.infobip.tcp.ping.exceptions.CatcherException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.sql.Timestamp;
import java.util.Date;

import static com.infobip.tcp.ping.util.Constants.MAX_ALLOWED_SIZE;
import static com.infobip.tcp.ping.util.Creator.initInetAddress;
import static com.infobip.tcp.ping.util.Creator.initSocket;

@Slf4j
public class CatcherThread extends Thread {

    private DatagramSocket socket;
    private boolean catchingPermitted;

    public CatcherThread(int port, String bind) {
        super("CatcherThread");
        socket = initSocket(port, initInetAddress(bind));
        catchingPermitted = true;
        log.info("Waiting for requests.");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            catchingPermitted = false;
            socket.close();
        }));
    }

    @Override
    public void run() {
        while (catchingPermitted) {
            byte[] bytes = new byte[MAX_ALLOWED_SIZE];
            DatagramPacket receivedPacket = receivePacket(bytes);

            ByteBuffer packetReceivedInfo = ByteBuffer.wrap(bytes);
            int seqNum = packetReceivedInfo.getInt();
            byte[] timestamp = new Timestamp(new Date().getTime()).toString().getBytes();

            ByteBuffer responseByteBuffer = ByteBuffer.allocate(receivedPacket.getLength());
            responseByteBuffer.putInt(seqNum);
            responseByteBuffer.putInt(timestamp.length);
            responseByteBuffer.put(timestamp);
            sendAnswer(receivedPacket, responseByteBuffer);
        }
    }

    private DatagramPacket receivePacket(byte[] bytes) {
        DatagramPacket packet = new DatagramPacket(bytes, MAX_ALLOWED_SIZE);
        try {
            socket.receive(packet);
        } catch (IOException e) {
            String message = "Error during receiving messages on catcher side";
            log.error(message, e);
            throw new CatcherException(message, e);
        }
        return packet;
    }

    private void sendAnswer(DatagramPacket receivedPacket, ByteBuffer responseByteBuffer) {
        byte[] bytes;
        bytes = responseByteBuffer.array();
        InetAddress address = receivedPacket.getAddress();
        int port = receivedPacket.getPort();
        DatagramPacket responsePacket = new DatagramPacket(bytes, receivedPacket.getLength(), address, port);
        try {
            socket.send(responsePacket);
        } catch (IOException e) {
            String message = "Error during sending answer from catcher";
            log.error(message, e);
            throw new CatcherException(message, e);
        }
    }

}
