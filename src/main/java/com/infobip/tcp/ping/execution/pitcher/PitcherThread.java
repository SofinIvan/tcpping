package com.infobip.tcp.ping.execution.pitcher;

import com.infobip.tcp.ping.model.Notice;
import lombok.extern.slf4j.Slf4j;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static com.infobip.tcp.ping.util.Creator.initInetAddress;
import static com.infobip.tcp.ping.util.Creator.initSocket;

@Slf4j
public class PitcherThread extends Thread {

    private int port;
    private int size;
    private int mps;
    private AtomicInteger secondCounter;
    private AtomicBoolean stopProcess;
    private InetAddress address;
    private DatagramSocket socket;
    private Map<Integer, Notice> sentPackets;
    private Map<Integer, Notice> receivedPackets;
    private Map<Integer, Timestamp> responseReceivedTimes;

    public PitcherThread(int port, int size, int mps, String hostname) {
        super("PitcherThread");
        this.port = port;
        this.size = size;
        this.mps = mps;
        secondCounter = new AtomicInteger(0);
        sentPackets = new ConcurrentHashMap<>();
        receivedPackets = new ConcurrentHashMap<>();
        responseReceivedTimes = new ConcurrentHashMap<>();
        address = initInetAddress(hostname);
        socket = initSocket(port);
        this.stopProcess = new AtomicBoolean(false);
    }

    @Override
    public void run() {
        int sendDelay = 0;
        int sendPeriod = 1000 / mps;
        int statisticPeriod = 1000;
        int statisticDelay = 1000;

        Timer timer = new Timer();
        ReceiveThread receiveThread = new ReceiveThread(size,
                receivedPackets,
                responseReceivedTimes,
                socket,
                secondCounter,
                stopProcess);
        receiveThread.start();

        SendTask sendTask = new SendTask(socket,
                sentPackets,
                address,
                port,
                secondCounter,
                size,
                stopProcess);
        StatisticsPerSecondTask statisticsPerSecondTask = new StatisticsPerSecondTask(sentPackets,
                receivedPackets,
                responseReceivedTimes,
                socket,
                secondCounter,
                timer,
                stopProcess);

        timer.schedule(sendTask, sendDelay, sendPeriod);
        timer.schedule(statisticsPerSecondTask, statisticDelay, statisticPeriod);
    }

}
