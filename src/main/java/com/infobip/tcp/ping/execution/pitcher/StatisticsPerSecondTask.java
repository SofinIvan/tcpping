package com.infobip.tcp.ping.execution.pitcher;

import com.infobip.tcp.ping.model.Notice;

import java.net.DatagramSocket;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Math.abs;

class StatisticsPerSecondTask extends TimerTask {

    private Map<Integer, Notice> sentPackets;
    private Map<Integer, Notice> receivedPackets;
    private Map<Integer, Timestamp> responseReceivedTimes;
    private DatagramSocket socket;
    private final AtomicBoolean stopProcess;
    private AtomicInteger secondCounter;
    private boolean countingPermitted;
    private Timer timer;

    public StatisticsPerSecondTask(Map<Integer, Notice> sentPackets,
                                   Map<Integer, Notice> receivedPackets,
                                   Map<Integer, Timestamp> responseReceivedTimes,
                                   DatagramSocket socket,
                                   AtomicInteger secondCounter,
                                   Timer timer,
                                   AtomicBoolean stopProcess) {
        this.sentPackets = sentPackets;
        this.receivedPackets = receivedPackets;
        this.responseReceivedTimes = responseReceivedTimes;
        this.socket = socket;
        this.secondCounter = secondCounter;
        this.timer = timer;
        this.countingPermitted = true;
        this.stopProcess = stopProcess;
    }

    // P1 - Moment when pitcher sends the packet - "sentPackets" keeps these records
    // C1 - Moment when catcher receives the packet
    // C2 - Moment when catcher sends the response - "receivedPackets" keeps these records
    // P2 - Moment when pitcher receives the response - "responseReceivedTimes" keeps these records

    @Override
    public void run() {
        if (countingPermitted) {
            secondCounter.incrementAndGet();
            System.out.println();
            System.out.println(new SimpleDateFormat("hh:mm:ss").format(new Date()) + ": ");
            System.out.println("Sent packets in second before: " + getSentPacketsPerSec());
            System.out.println("Received packets in second before: " + getReceivedPacketsPerSec());
            System.out.println("Average A-B: " + avgAtoBperSec());
            System.out.println("Average B-A: " + avgBtoAperSec());
            System.out.println("Average A-B-A: " + avgAtoBtoAperSec());
            System.out.println("Total Max A-B: " + totalMaxAtoB());
            System.out.println("Total Max B-A: " + totalMaxBtoA());
            System.out.println("Total max A-B-A: " + totalMaxAtoBtoA());
            //System.out.println("Sent packets: " + sentPackets.toString());
            if (secondCounter.get() > 10) {
                System.out.println("-------------------------------------------------");
                System.out.println("Received packets: " + receivedPackets.size());
                System.out.println("Sent packets: " + sentPackets.size());
                System.out.println("Lost packets: " + (sentPackets.size() - receivedPackets.size()));
                timer.cancel();
                stopProcess.set(true);
                socket.close();
                System.exit(0);
            }
        }
    }

    private int getSentPacketsPerSec() {
        int counter = 0;
        for (Notice notice : sentPackets.values()) {
            if (notice.getSecondCounter() == (secondCounter.get() - 1))
                counter++;
        }
        return counter;
    }

    private int getReceivedPacketsPerSec() {
        int counter = 0;
        for (Notice notice : receivedPackets.values()) {
            if (notice.getSecondCounter() == (secondCounter.get() - 1))
                counter++;
        }
        return counter;
    }

    private int avgAtoBperSec() {
        int sum = 0;
        int counter = 0;
        for (Map.Entry<Integer, Notice> entry : receivedPackets.entrySet()) {
            if (entry.getValue().getSecondCounter() == (secondCounter.get() - 1)) {
                long receivedTime = entry.getValue().getTimeStamp().getTime();
                long sentTime = sentPackets.get(entry.getKey()).getTimeStamp().getTime();
                sum += abs(receivedTime - sentTime);
                counter++;
            }
        }
        return counter == 0 ? 0 : sum / counter;
    }

    private int avgBtoAperSec() {
        int sum = 0;
        int counter = 0;

        for (Map.Entry<Integer, Notice> entry : receivedPackets.entrySet()) {
            if (entry.getValue().getSecondCounter() == (secondCounter.get() - 1)) {
                long responseTime = responseReceivedTimes.get(entry.getKey()).getTime();
                long receivedTime = entry.getValue().getTimeStamp().getTime();
                sum += abs(responseTime - receivedTime);
                counter++;
            }
        }
        return counter == 0 ? 0 : sum / counter;
    }

    private int avgAtoBtoAperSec() {
        long sum = 0;
        int counter = 0;

        for (Map.Entry<Integer, Timestamp> entry : responseReceivedTimes.entrySet()) {
            if (sentPackets.get(entry.getKey()).getSecondCounter() == (secondCounter.get() - 1)) {
                long responseTime = entry.getValue().getTime();
                long sentTime = sentPackets.get(entry.getKey()).getTimeStamp().getTime();
                sum += responseTime - sentTime;
                counter++;
            }
        }
        return counter == 0 ? 0 : (int) sum / counter;
    }

    private long totalMaxAtoB() {
        long max = 0;
        for (Map.Entry<Integer, Notice> entry : receivedPackets.entrySet()) {
            long receivedTime = entry.getValue().getTimeStamp().getTime();
            long sentTime = sentPackets.get(entry.getKey()).getTimeStamp().getTime();
            long diff = receivedTime - sentTime;
            max = diff > max ? diff : max;
        }
        return max;
    }

    private long totalMaxBtoA() {
        long max = 0;
        for (Map.Entry<Integer, Notice> entry : receivedPackets.entrySet()) {
            if (entry.getValue().getSecondCounter() == (secondCounter.get() - 1)) {
                long responseTime = responseReceivedTimes.get(entry.getKey()).getTime();
                long receivedTime = entry.getValue().getTimeStamp().getTime();
                long diff = responseTime - receivedTime;
                max = diff > max ? diff : max;
            }
        }
        return max;
    }

    private long totalMaxAtoBtoA() {
        long max = 0;
        for (Map.Entry<Integer, Timestamp> entry : responseReceivedTimes.entrySet()) {
            if (sentPackets.get(entry.getKey()).getSecondCounter() == (secondCounter.get() - 1)) {
                long diff = entry.getValue().getTime() - sentPackets.get(entry.getKey()).getTimeStamp().getTime();
                max = diff > max ? diff : max;
            }
        }
        return max;
    }

}