package com.infobip.tcp.ping.execution.pitcher;

import com.infobip.tcp.ping.exceptions.SendingException;
import com.infobip.tcp.ping.model.Notice;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
class SendTask extends TimerTask {

    private int seqNum = 0;
    private int bufLength;
    private final AtomicBoolean stopProcess;
    private AtomicInteger secondCounter;
    private InetAddress address;
    private int port;
    private Map<Integer, Notice> sentPackets;
    private DatagramSocket socket;

    public SendTask(DatagramSocket socket,
                    Map<Integer, Notice> sentPackets,
                    InetAddress address,
                    int port,
                    AtomicInteger secondCounter,
                    int bufLength, AtomicBoolean stopProcess) {
        this.bufLength = bufLength;
        this.secondCounter = secondCounter;
        this.address = address;
        this.port = port;
        this.sentPackets = sentPackets;
        this.socket = socket;
        this.stopProcess = stopProcess;
    }

    @Override
    public void run() {
        if (!stopProcess.get()) {
            ByteBuffer byteBuffer = ByteBuffer.allocate(bufLength);
            byteBuffer.putInt(seqNum);
            Timestamp ts = new Timestamp(new Date().getTime());
            byte[] timestamp = ts.toString().getBytes();
            byteBuffer.putInt(timestamp.length);
            byteBuffer.put(timestamp);
            byte[] byteBufferArray = byteBuffer.array();

            Notice notice = new Notice(seqNum, ts, secondCounter.get());
            sentPackets.put(seqNum, notice);
            DatagramPacket packet = new DatagramPacket(byteBufferArray, byteBufferArray.length, address, port);
            try {
                socket.send(packet);
            } catch (IOException e) {
                String message = "Error during sending";
                log.error(message);
                throw new SendingException(message, e);
            }
            seqNum++;
        }
    }
}