package com.infobip.tcp.ping.execution.pitcher;

import com.infobip.tcp.ping.exceptions.ReceivingException;
import com.infobip.tcp.ping.model.Notice;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
class ReceiveThread extends Thread {

    private int size;
    private Map<Integer, Notice> receivedPackets;
    private Map<Integer, Timestamp> responseReceivedTimes;
    private DatagramSocket socket;
    private AtomicInteger secondCounter;
    private AtomicBoolean stopProcess;

    public ReceiveThread(int size,
                         Map<Integer, Notice> receivedPackets,
                         Map<Integer, Timestamp> responseReceivedTimes,
                         DatagramSocket socket,
                         AtomicInteger secondCounter, AtomicBoolean stopProcess) {
        super("ReceiveThread");
        this.size = size;
        this.receivedPackets = receivedPackets;
        this.responseReceivedTimes = responseReceivedTimes;
        this.socket = socket;
        this.secondCounter = secondCounter;
        this.stopProcess = stopProcess;
    }

    @Override
    public void run() {
        while (!stopProcess.get()) {
            byte[] buf = new byte[size];
            DatagramPacket packet = new DatagramPacket(buf, size);
            receiveFromSocket(packet);
            ByteBuffer packetReceivedInfo = ByteBuffer.wrap(buf);
            Integer seqNumReceived = packetReceivedInfo.getInt();
            responseReceivedTimes.put(seqNumReceived, new Timestamp(new Date().getTime()));
            int timestampLength = packetReceivedInfo.getInt();
            byte[] ts = new byte[timestampLength];
            packetReceivedInfo.get(ts, 0, timestampLength);
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Timestamp timestamp = parseTimestamp(ts, s);
            Notice notice = new Notice(seqNumReceived, timestamp, secondCounter.get());
            receivedPackets.put(seqNumReceived, notice);
        }
    }

    private void receiveFromSocket(DatagramPacket packet) {
        try {
            socket.receive(packet);
        } catch (IOException e) {
            String message = "Exception during receiving from socket";
            log.error(message, e);
            throw new ReceivingException(message, e);
        }
    }

    private Timestamp parseTimestamp(byte[] ts, SimpleDateFormat s) {
        Timestamp timestamp;
        try {
            timestamp = new Timestamp(s.parse(new String(ts)).getTime());
        } catch (ParseException e) {
            String message = "Parse exception during timestamp parsing";
            log.error(message, e);
            throw new ReceivingException(message, e);
        }
        return timestamp;
    }

}
