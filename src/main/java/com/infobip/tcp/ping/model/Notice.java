package com.infobip.tcp.ping.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
public class Notice {

    private int seqNumber = 0;
    private Timestamp timeStamp;
    private int secondCounter = 0;
}
