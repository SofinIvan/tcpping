package com.infobip.tcp.ping.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    public static final String P = "p";
    public static final String C = "c";
    public static final String PITCHER_DESCRIPTION = "PitcherHelper mode";
    public static final String CATCHER_DESCRIPTION = "Catcher mode";
    public static final String PORT = "port";
    public static final String PORT_DESCRIPTION = "[PitcherHelper] TCP socket post used for connecting [Catcher] TCP socket port used for listening";
    public static final String BIND = "bind";
    public static final String BIND_DESCRIPTION = "[Catcher] TCP socket bind address that will be used to run listen";
    public static final String MPS = "mps";
    public static final String MPS_DESCRIPTION = "[PitcherHelper] the speed of message sending expressed as 'messages per second', default: 1";
    public static final String SIZE = "size";
    public static final String SIZE_DESCRIPTION = "[PitcherHelper] message length minimum: 50 maximum: 3000 default: 300";
    public static final String HOSTNAME_DESCRIPTION = "[PitcherHelper] the name of the computer which runs Catcher";

    public static final int DEFAULT_MPS = 1;
    public static final int DEFAULT_SIZE = 300;
    public static final int MAX_ALLOWED_SIZE = 3000;
}
