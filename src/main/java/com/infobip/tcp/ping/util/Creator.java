package com.infobip.tcp.ping.util;

import com.infobip.tcp.ping.exceptions.InvalidCommandLineArgumentsException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class Creator {

    public static DatagramSocket initSocket(int port) {
        DatagramSocket socket;
        try {
            socket = new DatagramSocket(port);
        } catch (SocketException e) {
            String message = "Error during socket connecting";
            log.error(message, e);
            throw new InvalidCommandLineArgumentsException(message, e);
        }
        return socket;
    }

    public static DatagramSocket initSocket(int port, InetAddress address) {
        DatagramSocket socket;
        try {
            socket = new DatagramSocket(port, address);
        } catch (SocketException e) {
            String message = "Error during socket connecting";
            log.error(message, e);
            throw new InvalidCommandLineArgumentsException(message, e);
        }
        return socket;
    }

    public static InetAddress initInetAddress(String hostname) {
        InetAddress address;
        try {
            address = InetAddress.getByName(hostname);
        } catch (UnknownHostException e) {
            String message = "Error during hostname resolving";
            log.error(message, e);
            throw new InvalidCommandLineArgumentsException(message, e);
        }
        return address;
    }

}
