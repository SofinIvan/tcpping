package com.infobip.tcp.ping.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@RequiredArgsConstructor
@ToString
public class CommandLineParameters {

    private final boolean isPitcher;
    private final boolean isCatcher;

    private final int port;
    private final String bind;

    private final int mps;
    private final int size;
    private final String hostname;
}
