package com.infobip.tcp.ping.util;

import com.infobip.tcp.ping.exceptions.InvalidCommandLineArgumentsException;
import com.infobip.tcp.ping.util.CommandLineParameters;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import static com.infobip.tcp.ping.util.Constants.BIND;
import static com.infobip.tcp.ping.util.Constants.BIND_DESCRIPTION;
import static com.infobip.tcp.ping.util.Constants.C;
import static com.infobip.tcp.ping.util.Constants.CATCHER_DESCRIPTION;
import static com.infobip.tcp.ping.util.Constants.DEFAULT_MPS;
import static com.infobip.tcp.ping.util.Constants.DEFAULT_SIZE;
import static com.infobip.tcp.ping.util.Constants.MPS;
import static com.infobip.tcp.ping.util.Constants.MPS_DESCRIPTION;
import static com.infobip.tcp.ping.util.Constants.P;
import static com.infobip.tcp.ping.util.Constants.PITCHER_DESCRIPTION;
import static com.infobip.tcp.ping.util.Constants.PORT;
import static com.infobip.tcp.ping.util.Constants.PORT_DESCRIPTION;
import static com.infobip.tcp.ping.util.Constants.SIZE;
import static com.infobip.tcp.ping.util.Constants.SIZE_DESCRIPTION;
import static org.apache.commons.lang3.math.NumberUtils.isCreatable;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CommandLineReader {

    public static CommandLineParameters readParametersFromCommandLine(String[] args) throws ParseException {
        Options options = initOptions();
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(options, args);
        validate(commandLine);
        return initCommandLineParameters(commandLine);
    }

    private static CommandLineParameters initCommandLineParameters(CommandLine commandLine) {
        return new CommandLineParameters(
                commandLine.hasOption(P),
                commandLine.hasOption(C),
                Integer.parseInt(commandLine.getOptionValue(PORT)),
                commandLine.getOptionValue(BIND),
                initOrGetDefaultIntOption(commandLine, MPS, DEFAULT_MPS),
                initOrGetDefaultIntOption(commandLine, SIZE, DEFAULT_SIZE),
                commandLine.hasOption(P) ? commandLine.getArgs()[0] : null
        );
    }

    private static int initOrGetDefaultIntOption(CommandLine commandLine, String option, int defaultInt) {
        return commandLine.getOptionValue(option) == null ? defaultInt : Integer.parseInt(commandLine.getOptionValue(option));
    }

    private static Options initOptions() {
        Options options = new Options();
        options.addOption(new Option(P, false, PITCHER_DESCRIPTION));
        options.addOption(new Option(C, false, CATCHER_DESCRIPTION));
        options.addOption(new Option(PORT, true, PORT_DESCRIPTION));
        options.addOption(new Option(BIND, true, BIND_DESCRIPTION));
        options.addOption(new Option(MPS, true, MPS_DESCRIPTION));
        options.addOption(new Option(SIZE, true, SIZE_DESCRIPTION));
        return options;
    }

    private static void validate(CommandLine commandLine) {
        validatePitcherOrCatcher(commandLine);
        if (commandLine.hasOption(P)) {
            validatePitcherArgs(commandLine);
        } else {
            validateCatcherArgs(commandLine);
        }
    }

    private static void validatePitcherArgs(CommandLine commandLine) {
        validatePort(commandLine);
        validateMps(commandLine);
        validateSize(commandLine);
        validateHostname(commandLine);
    }

    private static void validateCatcherArgs(CommandLine commandLine) {
        validatePort(commandLine);
        validateBind(commandLine);
    }

    private static void validateBind(CommandLine commandLine) {
        String bind = commandLine.getOptionValue(BIND);
        if (bind == null) {
            String invalidBindArg = "Invalid bind argument: -bind = null";
            throw new InvalidCommandLineArgumentsException(invalidBindArg);
        }
    }

    private static void validateHostname(CommandLine commandLine) {
        String[] commandLineArgs = commandLine.getArgs();
        if (commandLineArgs == null || commandLineArgs[0] == null) {
            String invalidHostnameArg = "Invalid hostname argument: hostname = null";
            throw new InvalidCommandLineArgumentsException(invalidHostnameArg);
        }
    }

    private static void validateSize(CommandLine commandLine) {
        String size = commandLine.getOptionValue(SIZE);
        if (size != null) {
            boolean creatable = isCreatable(size);
            if (creatable) {
                int messageSize = Integer.parseInt(size);
                if (messageSize < 50 || messageSize > 3000) {
                    String invalidMessageSize = "Invalid size argument: -size '%s' (size must be < 50 or more than 3000)";
                    throw new InvalidCommandLineArgumentsException(String.format(invalidMessageSize, messageSize));
                }
            } else {
                String invalidSize = "Invalid size argument: -mps '%s'";
                throw new InvalidCommandLineArgumentsException(String.format(invalidSize, size));
            }
        }
    }

    private static void validateMps(CommandLine commandLine) {
        String mps = commandLine.getOptionValue(MPS);
        if ((mps != null) && (!isCreatable(mps) || (Integer.parseInt(mps) <= 0))) {
            String invalidMpsArg = "Invalid mps argument: -mps '%s'";
            throw new InvalidCommandLineArgumentsException(String.format(invalidMpsArg, mps));
        }
    }

    private static void validatePort(CommandLine commandLine) {
        String port = commandLine.getOptionValue(PORT);
        if (!isCreatable(port)) {
            String invalidPortArg = "Invalid port argument: -port '%s'";
            throw new InvalidCommandLineArgumentsException(String.format(invalidPortArg, port));
        }
    }

    private static void validatePitcherOrCatcher(CommandLine cmd) {
        boolean isPitcherOrIsCatcher = (cmd.hasOption(P) && !cmd.hasOption(C)) || (!cmd.hasOption(P) && cmd.hasOption(C));
        if (!isPitcherOrIsCatcher) {
            String invalidArguments = "Invalid arguments, wrong mode: -p '%b' , -c '%b'";
            throw new InvalidCommandLineArgumentsException(String.format(invalidArguments, cmd.hasOption(P), cmd.hasOption(C)));
        }
    }

}
